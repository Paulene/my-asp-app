﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
namespace mabunga_Anonuevo
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        SqlConnection sqlCon = new SqlConnection("Data Source=DESKTOP-FCQ2RRJ\\SQLSERVER;Initial Catalog=ASP.CRUD;Integrated Security=True");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnDelete.Enabled = false;
                FillGridView();
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
        public void Clear()
        {
            hfContactID.Value = "";
            txtname.Text = txtmobile.Text = txtaddress.Text = "";
            lblSuccessMessage.Text = lblErrorMessage.Text = "";
            btnSave.Text = "Save";
            btnDelete.Enabled = false;

        }
       
        protected void lnk_OnClick(object sender, EventArgs e)
        {
            int ContactID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("tblinfoViewByID", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.Parameters.AddWithValue("@ContactID", ContactID);
            DataTable dtbl = new DataTable();
            sqlDa.Fill(dtbl);
            sqlCon.Close();
            hfContactID.Value = ContactID.ToString();
            txtname.Text = dtbl.Rows[0]["Name"].ToString();
            txtmobile.Text = dtbl.Rows[0]["Mobile"].ToString();
            txtaddress.Text = dtbl.Rows[0]["Address"].ToString();
            btnSave.Text = "Update";
            btnDelete.Enabled = true;


        }


        protected void btnDelete_Click1(object sender, EventArgs e)
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("tblinfoDeleteByID", sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@ContactID", Convert.ToInt32(hfContactID.Value));
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
            Clear();
            FillGridView();
            lblSuccessMessage.Text = "Deleted Successfully";


        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {
             if(sqlCon.State == ConnectionState.Closed)
            sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("tblinfoCreateOrUpdate", sqlCon);
            sqlCmd.CommandType= CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@ContactID", (hfContactID.Value ==""?0:Convert.ToInt32(hfContactID.Value)));
            sqlCmd.Parameters.AddWithValue("@Name", txtname.Text.Trim());
            sqlCmd.Parameters.AddWithValue("@Mobile",txtmobile.Text.Trim());
            sqlCmd.Parameters.AddWithValue("@Address",txtaddress.Text.Trim());
            sqlCmd.ExecuteNonQuery();
            sqlCon.Close();
            string ContactID = hfContactID.Value;
            Clear();
            if (hfContactID.Value == "")
                lblSuccessMessage.Text = "Saved Successfully";
            else
                lblSuccessMessage.Text = "Updated Successfully";

            FillGridView();

        }
        void FillGridView()
        {
            if (sqlCon.State == ConnectionState.Closed)
                sqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("tblinfoViewAll", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dtbl = new DataTable();
            sqlDa.Fill(dtbl);
            sqlCon.Close();
            gvtblinfo.DataSource = dtbl;
            gvtblinfo.DataBind();


        }

        
    }
}