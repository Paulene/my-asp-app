﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tblinfo.aspx.cs" Inherits="mabunga_Anonuevo.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="hfContactID" runat="server" />
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                </td>
                 <td colspan = "2">
                     <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                </td>
            
            </tr>
        

         <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Mobile"></asp:Label>
                </td>
                 <td colspan = "2">
                     <asp:TextBox ID="txtmobile" runat="server"></asp:TextBox>
                </td>
            
            </tr>

             <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Address"></asp:Label>
                </td>
                 <td colspan = "2">
                     <asp:TextBox ID="txtaddress" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            
            </tr>

            <tr>
                <td>
                   
                </td>
                 <td colspan = "2">
                     <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click1" />
                     <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                         onclick="btnDelete_Click1" />
                     <asp:Button ID="btnClear" runat="server" Text="Clear" 
                         onclick="btnClear_Click" Height="17px" />
                </td>
            
            </tr>

             <tr>
                <td>
                   
                </td>
                 <td colspan = "2">
                     <asp:Label ID="lblSuccessMessage" runat="server" Text="" ForeColor= "Green"></asp:Label>
                </td>
            
            </tr>

             <tr>
                <td>
                   
                </td>
                 <td colspan = "2">
                      <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor= "Red"></asp:Label>
                </td>
            
            </tr>
        </table>
        <br />
        <asp:GridView ID="gvtblinfo" runat="server" AutoGenerateColumns= "false" 
            selectedindexchanged="gvtblinfo_SelectedIndexChanged">
        <Columns>
            <asp:Boundfield Datafield= "Name" Headertext= "Name" />
            <asp:Boundfield Datafield= "Mobile" Headertext= "Mobile" />
            <asp:Boundfield Datafield= "Address" Headertext= "Address" />

            <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID= "lnkview" runat="server" CommandArgument= '<% #Eval ("ContactID") %>' OnClick="lnk_OnClick"> View</asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>

